import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np

Min = 987654321
Min_k = 0
k_vector = []
error_vector = []
#fig, ax = plt.subplots(figsize = (5, 5))


def k_sampling_validity(k):
	global Min, Min_k
	global fp

	Area = 7.56812e15
	lam, p, Inten = k_sampling(k)

	real_p = Inten/Area

	error = p - real_p
	avg_error = np.sum(error)/len(error)

	if(abs(avg_error) < Min):
		Min = avg_error
		Min_k = k

	print("\nk: " + str(k) + " " + str(avg_error))
	#fp.write(str(k) + "\t" + str(avg_error) + "\n")

	k_vector.append(k)
	error_vector.append(avg_error)




def k_sampling(k):
	#k = int(input("\nenter value of k: "))

	# Make domain(lambda)
	lam = np.arange(370, 680, (679-370)/(k - 1))
	#Calculate Intensity
	Inten = (1.1916e29)/(lam**5)/(np.exp(2.493e3/lam)-1)
	
	#Calculate sum of intensity
	temp_sum = np.sum(Inten)
	p = Inten/temp_sum

	return (lam, p, Inten)

def export_result(k):
	lam, p, Inten = k_sampling(k)

	file_name = "K-Sampling" + "(" + str(k) + ")" + ".txt"
	f = open(file_name, "w")

	line = ""
	for lam_x in lam:
		line += str(lam_x) + "\t"
	f.write(line + "\n")

	line = ""
	for p_x in p:
		line += str(p_x) + "\t"
	f.write(line + "\n")

	f.close()

def plot_result(k):
	fig, ax = plt.subplots(figsize = (5, 5))
	
	lam, p, Inten = k_sampling(k)
	ax.plot(lam, Inten, "o")
	lam = np.arange(370, 680, 1)
	Inten = (1.1916e29)/(lam**5)/(np.exp(2.493e3/lam)-1)
	#I2 = (1.1916e29)/(lam**5)/(np.exp(2.493e3*5772/4000/lam)-1)
	ax.plot(lam, Inten)
	#ax.plot(lam, I2)	

	ax.set_title("Blackbody Radiation K-Sampling(k = " + str(k) + ")")
	ax.set_xlabel("Wavelength [nm]")
	plt.show()



#fp = open("k_sampling_validity.txt", "w")
print("This is K-Sampling Program")

n = int(input("number of testcase: ")) # the number of test_case
for i in range(n):
	k = int(input("\nenter value of k: "))	
	export_result(k)
	k_sampling_validity(k)
	#plot_result(k)

print(Min, Min_k)

#fp.close()
'''
ax.plot(k_vector, error_vector)
ax.set_title("K-error result")
ax.set_xlabel("K")
ax.set_ylabel("p error")
plt.show()
'''















