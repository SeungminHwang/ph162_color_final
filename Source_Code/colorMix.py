import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import numpy as np
import random



def get_data():

	f = open("color_spec.txt", "r")

	lam = f.readline()
	lam = lam.split("\n")
	lam = lam[0].split("\t")

	R = f.readline()
	R = R.split("\n")
	R = R[0].split("\t")
	

	G = f.readline()
	G = G.split("\n")
	G = G[0].split("\t")
	

	Y = f.readline()
	Y = Y.split("\n")
	Y = Y[0].split("\t")

	O = f.readline()
	O = O.split("\n")
	O = O[0].split("\t")

	V = f.readline()
	V = V.split("\n")
	V = V[0].split("\t")

	B = f.readline()
	B = B.split("\n")
	B = B[0].split("\t")
	

	

	newO = []
	for i in range(len(lam)):
		newO.append([])
		lam[i] = int(lam[i])
		I = 1#(1.1916e29)/(lam[i]**5)/(np.exp(2.493e3/lam[i])-1)
		R[i] = (1 - float(R[i]))*I
		Y[i] = (1 - float(Y[i]))*I
		O[i] = (1 - float(O[i]))*I
		V[i] = (1 - float(V[i]))*I
		B[i] = (1 - float(B[i]))*I
		newO[i] = (0.5*R[i] + 0.5*B[i])

	f.close()

	fig, ax = plt.subplots(figsize = (4, 4))
	#ax.plot(lam, R, color = "red")
	#ax.plot(lam, B, color = "blue")
	ax.plot(lam, V, color = "purple")
	ax.plot(lam, newO, color = "green")

	ax.set_xlabel("wavelength (nm)")

	plt.show()


	return (lam, (R, G, Y))




data = get_data()


	
